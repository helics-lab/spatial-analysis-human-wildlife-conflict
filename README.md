# Spatial Analysis Human Wildlife Conflict

This repository contains supplementary code for our paper ‘A pan-African spatial assessment of human conflicts with lions and elephants’.

The scripts in this repository have been used to calculate the core indicators for our analysis. They are written in *GNU bash* (Bourne Again Shell, v5.1, [gnu.org/software/bash/](https://www.gnu.org/software/bash/bash.html)), Python (v3.8, [python.org](https://python.org/)) and SQL (PostgreSQL 13 + PostGIS 3, [postgresql.org](https://postgresql.org/), [postgis.net](https://postgis.net/)).

To replicate the analysis, first, download all input data sets. Each source directory in [indicators/data/](indicators/data/) contains a `source.txt` file that details the origin of the data set. With the exception of the lion and elephant range map data, which had to be obtained from the IUCN Cat Specialist Group and the IUCN African Elephant Specialist Group via personal communication, all data sources are freely available online. 

After obtaining all input data sets, navigate to [indicators/tools/](indicators/tools/). Adjust the configuration in both [01-import/00-configuration.sh](indicators/tools/01-import/00-configuration.sh) and [02-analysis/00-configuration.sh](indicators/tools/02-analysis/00-configuration.sh): most importantly, point `dbname`/`dbconnection` to a PostgreSQL instance with a PostGIS extension loaded.
Then, to run the import and analysis, call `runall.sh` in [indicators/tools/](indicators/tools/). Depending on your computer, this might take hours or days to complete.
