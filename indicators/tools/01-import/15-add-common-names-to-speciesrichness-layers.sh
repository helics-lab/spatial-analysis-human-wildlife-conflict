#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

# find common names
TEMPFILE="$(mktemp --suffix ".csv")"
cat <<EOF | psql "$dbconnection"
    \COPY (SELECT DISTINCT TRIM(binomial) FROM speciesrichness.herbivores_and_carnivores) TO '${TEMPFILE}' CSV;
EOF

while read BINOMIAL; do
    WIKIDATA_ID="$(wb query --property P225 --object "${BINOMIAL}")"
    COMMON_NAME="$(wb label "${WIKIDATA_ID}")"
    cat <<EOF | psql "$dbconnection"
        UPDATE
            speciesrichness.herbivores_and_carnivores
        SET
            commonname = '${COMMON_NAME}'
        WHERE
            binomial = '${BINOMIAL}';
EOF
done <"${TEMPFILE}"

rm -v "${TEMPFILE}"
