#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

dropdb -e "$dbname" 
createdb -e "$dbname" || exit -99

cat <<EOF | psql "$dbconnection"
    CREATE EXTENSION IF NOT EXISTS
        postgis;
    CREATE EXTENSION IF NOT EXISTS
        postgis_raster;
    CREATE EXTENSION IF NOT EXISTS
        dblink;

    -- create Africa_Albers_Equal_Area_Conic srs (for area calculation)
    INSERT INTO
        spatial_ref_sys
            (srid, proj4text)
        VALUES
            (
                102022,
                '+proj=aea +lat_1=20 +lat_2=-23 +lat_0=0 +lon_0=25 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'
            )
        ON CONFLICT DO NOTHING;
EOF
