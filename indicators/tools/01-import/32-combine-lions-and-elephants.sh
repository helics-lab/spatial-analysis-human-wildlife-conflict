#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

for i in " " " extended "; do
    (
        cat <<EOF | psql conflicts
                -- group into clusters
                DROP TABLE IF EXISTS
                    "lions and elephants combined${i}ranges";
                CREATE TABLE
                    "lions and elephants combined${i}ranges"
                AS
                    SELECT
                        ST_UnaryUnion(
                            unnest(
                                ST_ClusterWithin(
                                    geom,
                                    ${tolerance}
                                )
                            )
                        ) AS geom, 
                        0 AS lion_population,
                        0 AS elephant_population
                    FROM
                        (
                            SELECT
                                geom
                            FROM
                                "lion${i}ranges"
                            UNION SELECT
                                geom
                            FROM
                                "elephant${i}ranges"
                        ) t;

                CREATE INDEX ON 
                    "lions and elephants combined${i}ranges"
                USING
                    GIST(geom);

                UPDATE
                    "lions and elephants combined${i}ranges" c
                SET
                    lion_population =
                        (
                            SELECT
                                SUM(lion_population)
                            FROM
                                "lion${i}ranges" l
                            WHERE
                                ST_Intersects(
                                    l.geom,
                                    c.geom
                                )
                        ),
                    elephant_population = 
                        (
                            SELECT
                                SUM(elephant_population)
                            FROM
                                "elephant${i}ranges" e
                            WHERE
                                ST_Intersects(
                                    e.geom,
                                    c.geom
                                )
                        );

--            UPDATE
--                "lions and elephants combined${i}ranges"
--            SET
--                lion_population = 0
--            WHERE
--                lion_population IS NULL;
--
--            UPDATE
--                "lions and elephants combined${i}ranges"
--            SET
--                elephant_population = 0
--            WHERE
--                elephant_population IS NULL;


            ALTER TABLE
                "lions and elephants combined${i}ranges"
            ADD COLUMN
                gid SERIAL PRIMARY KEY,
            ADD COLUMN
                area_km2 FLOAT,
            ADD COLUMN
                centroid geometry('Point', 102022);

            UPDATE
                "lions and elephants combined${i}ranges"
            SET
                area_km2 = ROUND(
                    (ST_Area(geom) / 1000000.0)::numeric,
                    3
                ),
                centroid = ST_Centroid(geom);

            UPDATE
                "lions and elephants combined${i}ranges"
            SET
                geom = ST_Multi(ST_MakeValid(ST_Buffer(geom, 0.0)));

            WITH
                patch AS (
                    SELECT
                        ST_SetSRID(
                            ST_GeomFromText(
                                'Polygon ((1187270 -275980, 1152075 -321434, 1213243 -363512, 1246230 -317019, 1187270 -275980))'
                            ),
                            102022
                       ) AS geom
                )
            UPDATE
                "lions and elephants combined${i}ranges" r
            SET
                geom = ST_Union(r.geom, p.geom)
            FROM
                patch p
            WHERE
                r.geom && p.geom
            AND
                ST_Overlaps(r.geom, p.geom);

            WITH
                all_parts AS (
                    SELECT
                        gid,
                        ST_ExteriorRing(
                            (
                                ST_Dump(geom)
                            ).geom
                        ) AS geom
                    FROM
                        "lions and elephants combined${i}ranges"
                ),
                no_holes AS (
                    SELECT
                        gid,
                        ST_Collect(
                            ST_MakePolygon(
                                geom
                            )
                        ) AS geom
                    FROM
                        all_parts
                    GROUP BY
                        gid
                )
            UPDATE
                "lions and elephants combined${i}ranges" r
            SET
                geom = n.geom
            FROM
                no_holes n
            WHERE
                r.gid = n.gid;

EOF
    ) #&
done

wait
