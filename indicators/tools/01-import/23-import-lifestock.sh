#!/bin/bash

set +m

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cd "../../data/lifestock"

ramForEachProcess=$(expr $availableRam / 3)


for animal in sheep goats cattle; do
    echo $animal
    (
        tif="$(ls -1 ./${animal}/dataverse_files/5_??_2010_Da.tif)"
        tif102022="${tif/%.tif/_ESRI102022.tif}"

        gdalwarp \
            --config GDAL_CACHEMAX ${ramForEachProcess} \
            -co COMPRESS=ZSTD \
            -co ZLEVEL=1 \
            -overwrite \
            -cutline 'PG:dbname=conflicts' \
            -csql 'SELECT ST_Transform(geom, 4326) FROM naturalearthdata.africa' \
            -crop_to_cutline \
            -multi \
            "${tif}" \
            "${tif102022}"

	    echo "DROP TABLE IF EXISTS data.${animal};" | psql "$dbconnection"

        raster2pgsql -C -I -t 1024x1024 "${tif102022}" data."${animal}" | psql "$dbconnection"


        # add sheep another time to have an empty table set up
        # for summing up all lifestock (yes, make-shift, but it works)
        if [[ "$animal" == "sheep" ]]; then
            raster2pgsql -C -I "${tif102022}" data.lifestock | psql "$dbconnection"
        fi

    ) #& 
done

wait

cat <<EOF | psql "$dbconnection"
    UPDATE
        lifestock
    SET
        rast = (
            SELECT
                ST_Union(a.rast, 'SUM')
            FROM
                (
                    SELECT rast from sheep
                    UNION SELECT rast from goats
                    UNION SELECT rast from cattle
                ) a
        );
EOF
