#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cd "../../data/humans"

ramForEachProcess=$(expr $availableRam / 5)

for year in 2020; do  #2000 2005 2010 2015 2020; do
    (
        tif="$(ls -1 "gpw_v4_population_count_rev11_${year}_30_sec.tif" )"
        tif102022="${tif/%.tif/_ESRI102022.tif}"

        gdalwarp \
            --config GDAL_CACHEMAX ${ramForEachProcess} \
            -co COMPRESS=ZSTD \
            -co ZLEVEL=1 \
            -co PREDICTOR=2 \
            -overwrite \
            -cutline 'PG:dbname=conflicts' \
            -csql 'SELECT ST_Transform(geom, 4326) FROM naturalearthdata.africa' \
            -crop_to_cutline \
            -multi \
            "${tif}" \
            "${tif102022}"

        if [[ "${year}" == "2020" ]]; then
            year=""
        fi
	    echo "DROP TABLE data.humans${year};" | psql "${dbconnection}"

        raster2pgsql -C -I -t 1024x1024 "${tif102022}" data."humans${year}" | psql "$dbconnection"
    )
done

wait
