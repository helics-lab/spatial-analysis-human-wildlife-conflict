#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "$dbconnection"
    CREATE SCHEMA IF NOT EXISTS
        data;

    ALTER DATABASE
        ${dbname}
    SET
        search_path
    TO
        data, public;
EOF
