#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"
    DROP SCHEMA IF EXISTS quantiles CASCADE;
    CREATE SCHEMA quantiles;
EOF

for r in "humans" "cattle" "goats" "lifestock" "sheep" "crops"; do
    for q in 70 80 90; do
        (
            python ./36a-calculate-quantiles-for-threats.py \
                "${dbconnection}" \
                "${r}" \
                "${q}"
#        )&
#    done
#done
#wait
#
#for r in "humans" "cattle" "goats" "lifestock" "sheep" "crops"; do
#    for q in 90; do  #70 80 90; do
#        (
        cat <<EOF | psql "${dbconnection}"
            ALTER TABLE
                quantiles.${r}_q${q}
            RENAME COLUMN
                wkb_geometry
            TO
                geom;

            ALTER TABLE
                quantiles.${r}_q${q}
            ALTER COLUMN
                geom TYPE GEOMETRY;

            UPDATE
                quantiles.${r}_q${q}
            SET
                geom = ST_Transform(
                    ST_SetSRID(
                        geom,
                        4326
                    ),
                    102022
                );

            CREATE INDEX IF NOT EXISTS
                ${r}_q${q}_geom_idx
            ON
                quantiles.${r}_q${q}
            USING
                SPGIST(geom);

            WITH
                clipped_polygons AS (
                    SELECT
                        q.ogc_fid,
                        ST_Intersection(
                            ST_Buffer(q.geom, 0.0),
                            ST_Buffer(a.geom, 0.0)
                        ) AS geom
                    FROM
                        quantiles.${r}_q${q} q,
                        naturalearthdata.africa a
                    WHERE
                        q.geom && a.geom
                        AND NOT q.geom @ a.geom
                    GROUP BY
                        q.ogc_fid,
                        q.geom,
                        a.geom
                )
                UPDATE
                    quantiles.${r}_q${q} q
                SET
                    geom = p.geom
                FROM
                    clipped_polygons p
                WHERE
                    q.ogc_fid = p.ogc_fid;

            CREATE TEMPORARY TABLE
                polygons
            AS
                WITH
                    pns AS (
                        SELECT
                            q,
                            ST_Dump(geom) as geomdump
                        FROM
                            quantiles.${r}_q${q}
                    )
                SELECT
                    q,
                    (geomdump).geom AS geom
                FROM
                    pns;

            TRUNCATE
                quantiles.${r}_q${q};

            INSERT INTO
                quantiles.${r}_q${q}
                (
                    q,
                    geom
                )
                SELECT
                    q,
                    geom
                FROM
                    polygons;

            ALTER TABLE
                quantiles.${r}_q${q}
            ALTER COLUMN
                geom
            SET DATA TYPE
                GEOMETRY('Polygon', 102022);

            DROP INDEX IF EXISTS
                quantiles.${r}_q${q}_wkb_geometry_idx;
            DROP INDEX IF EXISTS
                quantiles.${r}_q${q}_geom_idx;
            CREATE INDEX
                ${r}_q${q}_geom_idx
            ON
                quantiles.${r}_q${q}
            USING
                SPGIST(geom);
EOF
        )&
    done
   done
wait
