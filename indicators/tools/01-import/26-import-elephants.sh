#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cd "../../data/elephant range/"

if [[ ! -e "africanelephantdatabase.gpkg" ]]; then
    aed-downloader
fi

ogr2ogr \
    -nlt PROMOTE_TO_MULTI \
    -overwrite \
    -s_srs EPSG:4326 \
    -t_srs "+proj=aea +lat_1=20 +lat_2=-23 +lat_0=0 +lon_0=25 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs" \
    -f PostgreSQL \
    "PG:${dbconnection}" \
    africanelephantdatabase.gpkg \
    2016


cat <<EOF | psql "$dbconnection"

    ALTER TABLE
        "2016"        
    ALTER COLUMN
        geom
    SET DATA TYPE geometry('MultiPolygon');
    
    UPDATE
        "2016"
    SET
        geom = ST_Transform(
            geom,
            102022
        );

    ALTER TABLE
        "2016"        
    ALTER COLUMN
        geom
    SET DATA TYPE geometry('MultiPolygon', 102022);


    DROP TABLE IF EXISTS
        elephants_per_stratum;

    CREATE TEMPORARY TABLE
        elephants_per_stratum
    AS
        SELECT
            inputsystem,
            aed_stratum,
            "number of elephants (estimate)",
            geom
        FROM
            (
                SELECT
                    *,
                    ROW_NUMBER() OVER (
                        PARTITION BY
                            geom
                        ORDER BY
                            "survey year" DESC,
                            "number of elephants (estimate)" DESC
                        ) AS row
                FROM
                    "2016"
            ) t
        WHERE
            row = 1;

    DELETE FROM
        elephants_per_stratum
    WHERE
        inputsystem = ANY(ARRAY['Rest of Mozambique range', 'Rest of Gabon Forest Range']);

    DROP TABLE IF EXISTS
        elephants_per_inputsystem;

    CREATE TEMPORARY TABLE
        elephants_per_inputsystem
    AS
        SELECT
            inputsystem,
            SUM("number of elephants (estimate)") AS elephant_count,
            ST_Buffer(
                ST_Union(
                    ST_Buffer(
                        geom,
                        ${tolerance}
                    )
                ),
                -${tolerance}
            ) AS geom
        FROM
            elephants_per_stratum
        GROUP BY
            inputsystem;

    CREATE INDEX ON
        elephants_per_inputsystem
    USING
        GIST(geom);

    -- cluster touching geometries
    CREATE TEMPORARY TABLE
        elephant_clusters
    AS
        SELECT
            ST_Buffer(
                ST_UnaryUnion(
                    unnest(
                        ST_ClusterIntersecting(
                            ST_Buffer(
                                geom,
                                ${tolerance}
                            )
                        )
                    )
                ),
                -${tolerance}
            ) AS geom
        FROM
            elephants_per_inputsystem;

    ALTER TABLE
        elephant_clusters 
    ADD COLUMN
        cluster SERIAL PRIMARY KEY;

    CREATE INDEX ON
        elephant_clusters
    USING
        GIST(geom);

    -- match input systems to clusters
    ALTER TABLE
        elephants_per_inputsystem
    ADD COLUMN
        cluster INT;

    UPDATE
        elephants_per_inputsystem e
    SET
        cluster = (
            SELECT
                cluster
            FROM
                elephant_clusters c
            WHERE
                ST_Intersects(
                    e.geom,
                    c.geom
                )
        );

    -- group together by cluster, and sum population
    DROP TABLE IF EXISTS
        "elephant ranges";
    CREATE TABLE
        "elephant ranges"
    AS
        SELECT
            SUM(elephant_count)::INT AS elephant_population,
            ST_Buffer(
                ST_Union(
                    ST_Buffer(
                        geom,
                        ${tolerance}
                    )
                ),
                -${tolerance}
            ) AS geom
        FROM
            elephants_per_inputsystem
        GROUP BY
            cluster;

    ALTER TABLE
        "elephant ranges"
    ADD COLUMN
        gid SERIAL PRIMARY KEY,
    ADD COLUMN
        area_km2 FLOAT,
    ADD COLUMN
        centroid geometry('Point', 102022);

    UPDATE
        "elephant ranges"
    SET
        area_km2 = ROUND(
            (
                ST_Area(geom) / 1000000.0
            )::numeric,
            3
        ),
        centroid = ST_Centroid(geom);

    CREATE INDEX ON
        "elephant ranges"
    USING
        GIST(geom);

    UPDATE
        "elephant ranges"
    SET
        geom = ST_Multi(
            ST_MakeValid(
                ST_Buffer(
                    geom
                    , 0.0
                )
            )
        );

    DROP TABLE
        "2016";

    -- remove holes and small external areas (<= 5km²)
    WITH
        exterior_polygons1
        AS (
            SELECT
                ST_MakePolygon(
                    ST_ExteriorRing(
                        (ST_Dump(geom)).geom
                    )
                ) as geom,
                (ST_Dump(geom)).path as path,
                gid
            FROM
                "elephant ranges"
        ),
        exterior_polygons2
        AS (
            SELECT
                ST_Union(geom) AS geom,
                gid
            FROM
                exterior_polygons1
            WHERE
                ST_Area(geom) >= 5000000
            AND
                path = '{1}'::INT[]
            GROUP BY
                gid
        )
    UPDATE
        "elephant ranges" r
    SET
        geom = e.geom
    FROM
        exterior_polygons2 e
    WHERE
    r.gid = e.gid;

    DROP TABLE IF EXISTS
        "elephant ranges multipart";

    ALTER TABLE
        "elephant ranges"
    RENAME TO
        "elephant ranges multipart";

    CREATE TABLE
        "elephant ranges"
    AS (
        SELECT
            ROUND(
                elephant_population *
                (
                    ST_Area((ST_Dump(geom)).geom) / 
                    ST_Area(geom)
                )
            )::BIGINT AS elephant_population,
            ST_Multi((ST_Dump(geom)).geom)::GEOMETRY('MultiPolygon', 102022) AS geom,
            (
                ST_Area((ST_Dump(geom)).geom) / 1000000
            )::FLOAT AS area_km2,
            ST_Centroid(geom)::GEOMETRY('Point', 102022) as centroid
        FROM
            "elephant ranges multipart"
        GROUP BY
            gid,
            geom,
            elephant_population
    );

    ALTER TABLE
        "elephant ranges"
    ADD COLUMN
        gid SERIAL;
        
    CREATE INDEX
        ON "elephant ranges"
        USING GIST(geom);
        
    DROP TABLE
        "elephant ranges multipart";

EOF

# elephant range data is of really poor quality
# if more than 50% of an elephant range overlaps
# with a protected area, change its geometry to 
# the protected area’s geometry

cat <<EOF | psql "$dbconnection"
    DROP TABLE IF EXISTS
        "elephant ranges and overlapping protected areas";

    CREATE TEMPORARY TABLE
        "elephant ranges and overlapping protected areas"
            AS (
                SELECT
                    gid,
                    geom AS e_geom,
                    NULL::GEOMETRY('MultiPolygon', 102022) AS p_geom,
                    NULL::GEOMETRY('MultiPolygon', 102022) AS result_geom
                FROM
                    "elephant ranges"
            );

    UPDATE
        "elephant ranges and overlapping protected areas" e
        SET p_geom =
            (
                SELECT
                    ST_Multi(
                        ST_Union(p.geom)
                    ) AS p_geom
                FROM
                    "protected areas" p
                WHERE
                    e.e_geom && p.geom
                AND
                    ST_Intersects(e.e_geom, p.geom)
                GROUP BY
                    e.e_geom
            );

    UPDATE
        "elephant ranges and overlapping protected areas"
    SET
        result_geom = CASE WHEN
            (p_geom IS NULL OR (ST_Area(ST_Intersection(e_geom, p_geom)) / ST_Area(e_geom)) <= 0.4)
        THEN
            ST_Multi(e_geom)
        ELSE
            ST_Multi(p_geom)
        END;

    UPDATE
        "elephant ranges" e
    SET
        geom = result_geom
    FROM
        "elephant ranges and overlapping protected areas" ep
    WHERE
        ep.gid = e.gid;

UPDATE
    "elephant ranges"
SET
    geom = ST_Multi(ST_MakeValid(geom));

EOF
