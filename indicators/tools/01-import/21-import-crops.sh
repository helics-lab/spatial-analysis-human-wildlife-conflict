#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh


temp="$(mktemp -d)"

cd "../../data/cropland/"

ls -1 [EW][0-9][0-9][0-9][NS][0-9][0-9]_ProbaV_LC100_epoch2015_global_v2.0.1_products_EPSG-4326.zip | while read zip; do

    (
        tif="${zip/products/crops-coverfraction-layer}"
        tif="${tif/%.zip/.tif}"

        unzip "${zip}" "${tif}" -d "${temp}"
        tif="${temp}/${tif}"
    )&
done

wait

sleep 5

gdalwarp \
    --config GDAL_CACHEMAX "${availableRam}" \
    -co COMPRESS=ZSTD \
    -co ZLEVEL=1 \
    -co PREDICTOR=2 \
    -overwrite \
    -dstnodata '255' \
    "${temp}"/[EW][0-9][0-9][0-9][NS][0-9][0-9]_ProbaV_LC100_epoch2015_global_v2.0.1_crops-coverfraction-layer_EPSG-4326.tif \
    ../crops_4326.tif

gdalwarp \
    --config GDAL_CACHEMAX "${availableRam}" \
    -co COMPRESS=ZSTD \
    -co ZLEVEL=1 \
    -co PREDICTOR=2 \
    -overwrite \
    -cutline 'PG:dbname=conflicts' \
    -csql 'SELECT ST_Transform(geom, 4326) AS geom FROM naturalearthdata.africa' \
    -crop_to_cutline \
    -tr 0.00992063492063 0.00992063492063 \
    -multi \
    ../crops_4326.tif \
    ../crops_ESRI102022.tif

#gdalwarp \
#    --config GDAL_CACHEMAX "${availableRam}" \
#    -co COMPRESS=ZSTD \
#    -co ZLEVEL=1 \
#    -co PREDICTOR=2 \
#    -overwrite \
#    -r average \
#    -tr 1000 1000 \
#    ../crops_ESRI102022.tif \
#    ../crops_ESRI102022_10km.tif

cat <<EOF | psql "$dbconnection"
    DROP TABLE IF EXISTS
        crops;
    DROP TABLE IF EXISTS
        crops;
EOF

#raster2pgsql -C -I -t 1024x1024 ../crops.tif public.crops | psql "$dbconnection"
raster2pgsql -C -I -t 1024x1024 ../crops_ESRI102022.tif data.crops | psql "$dbconnection"

rm -Rfv "${temp}"
