#!/usr/bin/env python

import os
import os.path
import sys
from osgeo import gdal, ogr, osr
import numpy as np
import numpy.ma as ma

gdal.UseExceptions()


def main():
    pwd = os.path.abspath(
        os.path.dirname(sys.argv[0])
    )
    try:
        dbConnection = sys.argv[1]
    except IndexError:
        return
    
    try:
        rasters = sys.argv[2].split(",")
    except IndexError:
        rasters = [
            "humans",
            "cattle",
            "goats",
            "lifestock",
            "sheep",
            "crops"
        ]
        
    try:
        quantiles = [int(s) for s in sys.argv[3].split(",")]
    except (IndexError, ValueError):
        #quantiles = [66] + [q for q in range(50, 100, 5)] + [96, 97, 98, 99]
        #quantiles = [95, 90, 80, 75, 70, 60, 50]
        quantiles = [70, 80, 90]
        quantiles = [q for q in reversed(sorted(quantiles))]

    for raster in rasters:
        quantile(dbConnection, raster, quantiles)

def quantile(dbConnection, raster, quantiles):
    
    raster_dsn = "PG:{dbconnection:s} schema=data table={raster:s} mode=2".format(
        dbconnection=dbConnection,
        raster=raster
    )
    
    dataset = gdal.Open(raster_dsn)
    srs = osr.SpatialReference()
    srs.ImportFromWkt(dataset.GetProjection())
    gt = dataset.GetGeoTransform()
    area = -1.0 * gt[1] * gt[5]
    dataset = None

    destinationVectorDataSource = ogr.Open(
        "PG:{dbconnection:s}".format(dbconnection=dbConnection)
    )

    for q in quantiles:
        dataset = gdal.Open(raster_dsn)
        band = dataset.GetRasterBand(1)
        BandType = gdal.GetDataTypeName(band.DataType)
        noDataValue = band.GetNoDataValue()
        data = band.ReadAsArray(0, 0, band.XSize, band.YSize)

        if raster == "crops":
            data = ma.masked_where(
                (data == noDataValue) | (data < 0) | (data > 100),
                data
            )
        else:
            data = ma.masked_where((data == noDataValue) | (data < 0), data)
        data = data.astype(float) * 1.0
        data = ma.filled(data, np.nan)

        threshold = np.nanpercentile(data, q)
        print(threshold)

        data = None
        band = None
        dataset = None

        if True:  # threshold > 0:

            path = "{dataset:s}_q{quantile:02d}.tif".format(
                dataset=raster,
                quantile=q
            )

            dataset = gdal.Open(raster_dsn)
            band = dataset.GetRasterBand(1)
            BandType = gdal.GetDataTypeName(band.DataType)
            data = band.ReadAsArray(0, 0, band.XSize, band.YSize)

            newDataset = gdal.GetDriverByName("MEM").CreateCopy(
                path,
                dataset,
                strict=0
            )

            newBand = newDataset.GetRasterBand(1)

            data[data == noDataValue] = -1
            data[np.isnan(data)] = -1
            if raster == "crops":
                data[data < 0] = -1
                data[data > 100] = -1
            newBand.WriteArray(
                np.where(
                    (data > threshold) & (data != -1),
                    np.ones_like(data, np.int8),
                    np.zeros_like(data, np.int8)
                )
            )

            layerName = "quantiles.{raster:s}_q{q:02d}".format(
                raster=raster,
                q=q
            )
            
            layer = destinationVectorDataSource.CreateLayer(
                layerName,
                srs
            )
            newField = ogr.FieldDefn("q", ogr.OFTInteger)
            layer.CreateField(newField)

            gdal.Polygonize(newBand, None, layer, 0, [], callback=None )

            for feature in layer:
                if feature.GetField("q") == 0:
                    layer.DeleteFeature(feature.GetFID())

            dataset = None
            band = None
            data = None
            newDataset = None
            threshold = None
            layer = None

        try:
            pass #os.unlink(path)
        except FileNotFoundError:
            pass

    destinationVectorDataSource = None
    dataset = None


if __name__ == "__main__":
    main()
