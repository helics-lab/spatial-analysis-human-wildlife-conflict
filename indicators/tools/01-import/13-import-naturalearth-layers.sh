#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "$dbconnection"

    BEGIN;

    CREATE SCHEMA IF NOT EXISTS
        naturalearthdata;

    SET search_path TO naturalearthdata, public;

    CREATE TABLE
        africa
    AS
        SELECT
            ST_MakeValid(
                ST_Multi(
                    ST_Buffer(
                        ST_Transform(
                            geom,
                            102022
                        ),
                        0.0
                    )
                )
            )::geometry('MultiPolygon', 102022) AS geom
        FROM
            DBLINK(
                'dbname=naturalearthdata',
                '
                    SELECT
                        ST_Multi(
                            ST_Union(
                                geom
                            )
                        ) AS geom
                    FROM
                        ne_10m_geography_regions_polys
                    WHERE
                        "featurecla" = ''Continent''
                    AND "name" = ''AFRICA''
                '
            )
        AS (
            geom geometry('MultiPolygon', 4326)
        );

    CREATE INDEX
        ON africa
        USING GIST(geom);

    ALTER TABLE
        africa
    ADD COLUMN
        id SERIAL PRIMARY KEY;

    END;


    BEGIN;

    CREATE TABLE
        countries
    AS
        SELECT
            ST_MakeValid(
                ST_Multi(
                    ST_Buffer(
                        ST_Transform(
                            geom,
                            102022
                        ),
                        0.0
                    )
                )
            )::geometry('MultiPolygon', 102022) AS geom,
            adm0_a3,
            name
        FROM
            DBLINK(
                'dbname=naturalearthdata',
                '
                    SELECT
                        ST_Multi(
                            ST_Union(
                                geom
                            )
                        ) AS geom,
                        adm0_a3,
                        name
                    FROM
                        ne_10m_admin_0_countries
                    WHERE
                        continent = ''Africa''
                    GROUP BY
                        adm0_a3,
                        name
                '
            )
        AS (
            geom geometry('MultiPolygon', 4326),
            adm0_a3 TEXT,
            name TEXT
        );

    CREATE INDEX
        ON countries
        USING GIST(geom);

    CREATE UNIQUE INDEX
        ON countries(adm0_a3);

    ALTER TABLE
        countries
    ADD COLUMN
        id SERIAL PRIMARY KEY;

COMMIT;

EOF
