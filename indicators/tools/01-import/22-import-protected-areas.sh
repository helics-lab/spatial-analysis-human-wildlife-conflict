#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cd "../../data/protected areas/"

currentRelease="$(date --date='-1 month' '+WDPA_%b%Y')"
# apparently there’s no release for every month
currentRelease="WDPA_May2019"
if [[ ! -e "${currentRelease}-shapefile.zip" ]]; then
    wget -c "https://protectedplanet.net/downloads/${currentRelease}?type=shapefile"
fi

ogr2ogr \
    -spat_srs "EPSG:4326" \
    -spat -20.5 -35.1 54.8 38.6 \
    -where "MARINE != '2'" \
    -nln "protected areas" \
    -nlt PROMOTE_TO_MULTI \
    -overwrite \
    -t_srs "+proj=aea +lat_1=20 +lat_2=-23 +lat_0=0 +lon_0=25 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs" \
    -f PostgreSQL \
    "PG:${dbconnection}" \
    "/vsizip/${currentRelease}-shapefile.zip" \
    ${currentRelease}-shapefile-polygons
    
cat <<EOF | psql "$dbconnection"

    ALTER TABLE
        "protected areas"
    RENAME COLUMN
        wkb_geometry
    TO
        geom;

    ALTER TABLE
        "protected areas"        
    ALTER COLUMN
        geom
    SET DATA TYPE geometry('MultiPolygon');
    
    UPDATE
        "protected areas"
    SET
        geom = ST_Transform(
            geom,
            102022
        );

    ALTER TABLE
        "protected areas"        
    ALTER COLUMN
        geom
    SET DATA TYPE geometry('MultiPolygon', 102022);

    ALTER TABLE
        "protected areas"
    ADD COLUMN
        wdpaid_int BIGINT;

    UPDATE
        "protected areas"
    SET
        wdpaid_int = wdpaid::INT;

    ALTER TABLE
        "protected areas"
    DROP CONSTRAINT
        "protected areas_pkey";

    ALTER TABLE
        "protected areas"
    ADD PRIMARY KEY (wdpaid_int);

    CREATE INDEX ON
        "protected areas"
    USING
        GIST(geom);

    DELETE FROM
        "protected areas"
    WHERE
        iucn_cat <> ALL(ARRAY['Ia', 'Ib', 'II', 'IV', 'Not Assigned', 'Not Reported'])
    OR
        desig_eng = 'UNESCO-MAB Biosphere Reserve'
    OR
        NOT ST_Intersects(
            geom,
            (
                SELECT
                    geom
                FROM
                    naturalearthdata.africa
                LIMIT
                    1
            )
        );

    UPDATE
        "protected areas"
    SET
        geom = 
            ST_MakeValid(
                ST_Multi(
                    ST_Buffer(
                        ST_Intersection(
                            geom, 
                            (
                                SELECT
                                    geom
                                FROM
                                    naturalearthdata.africa
                                LIMIT
                                    1
                            )
                        ),
                        0.0
                    )
                )
            );

    -- remove holes and small external areas (<= 5km²)
    WITH
        exterior_polygons1
        AS (
            SELECT
                ST_MakePolygon(
                    ST_ExteriorRing(
                        (ST_Dump(geom)).geom
                    )
                ) as geom,
                (ST_Dump(geom)).path as path,
                wdpaid
            FROM
                "protected areas"
        ),
        exterior_polygons2
        AS (
            SELECT
                ST_Union(geom) AS geom,
                wdpaid
            FROM
                exterior_polygons1
            WHERE
                ST_Area(geom) >= 5000000
            OR
                path = '{1}'::INT[]
            GROUP BY
                wdpaid
        )
    UPDATE
        "protected areas" p
    SET
        geom = ST_Multi(e.geom)
    FROM
        exterior_polygons2 e
    WHERE
    p.wdpaid = e.wdpaid;

EOF
