#!/usr/bin/env python

import os
import os.path
import sys
from osgeo import gdal, ogr, osr
import numpy as np
import numpy.ma as ma
import pandas

gdal.UseExceptions()


def main():
    db_connection = "dbname=conflicts"

    rasters = [
        "humans",
        "lifestock",
        "crops"
    ]

    quantiles = [q for q in range(0, 101)]

    output = []
    output.append([""] + quantiles)
    for raster in rasters:
        output.append([raster] + [q for q in compute_quantiles(db_connection, raster, quantiles)])

    df = pandas.DataFrame(output)
    df.to_csv("../../results/quantiles_values.csv")


def compute_quantiles(db_connection, raster, quantiles):
    print(raster)

    raster_dsn = "PG:{dbconnection:s} schema=data table={raster:s} mode=2".format(
        dbconnection=db_connection,
        raster=raster
    )

    dataset = gdal.Open(raster_dsn)
    band = dataset.GetRasterBand(1)
    no_data_value = band.GetNoDataValue()
    data = band.ReadAsArray(0, 0, band.XSize, band.YSize)

    if raster == "crops":
        data = ma.masked_where(
            (data == no_data_value) | (data < 0) | (data > 100),
            data
        )
    else:
        data = ma.masked_where((data == no_data_value) | (data < 0), data)
    data = data.astype(float) * 1.0
    data = ma.filled(data, np.nan)

    dataset = None
    band = None

    for q in quantiles:
        threshold = np.nanpercentile(data, q)
        print(q, threshold)
        yield threshold

    data = None


if __name__ == "__main__":
    main()
