#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cd "../../data/lion range/"

ogr2ogr \
    -nln lion_ranges \
    -nlt PROMOTE_TO_MULTI \
    -overwrite \
    -s_srs EPSG:4326 \
    -t_srs "+proj=aea +lat_1=20 +lat_2=-23 +lat_0=0 +lon_0=25 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs" \
    -dim XY \
    -f PostgreSQL \
    "PG:${dbconnection}" \
    Lion_Range_GCLA_2018.shp


cat <<EOF | psql "$dbconnection"

    ALTER TABLE
        lion_ranges
    RENAME COLUMN
        wkb_geometry
    TO
        geom;

    ALTER TABLE
        "lion_ranges"        
    ALTER COLUMN
        geom
    SET DATA TYPE geometry('MultiPolygon');
    
    UPDATE
        "lion_ranges"
    SET
        geom = ST_Transform(
            geom,
            102022
        );

    ALTER TABLE
        "lion_ranges"        
    ALTER COLUMN
        geom
    SET DATA TYPE geometry('MultiPolygon', 102022);


    -- trim string (yeah, dataset’s that messy)
    UPDATE lion_ranges
        SET 
            name2 = trim(name2, E' \\n\\r\\t'),
            country = trim(country, E' \\n\\r\\t'),
            region = trim(region, E' \\n\\r\\t');

    -- more crappy data
    UPDATE lion_ranges
        SET f2018_pop = 1386
    WHERE
        objectid_1 = 39;

    -- and even more
    DELETE FROM
        lion_ranges
    WHERE
        geom is NULL;

    DROP TABLE IF EXISTS lion_ranges_tmp0;
    DROP TABLE IF EXISTS lion_ranges_tmp1;
    DROP TABLE IF EXISTS lion_ranges_tmp2;
    DROP TABLE IF EXISTS lion_ranges_tmp3;

    -- clean case3
    CREATE TEMPORARY TABLE
        lion_ranges_tmp0
    AS
        SELECT
            string_agg(objectid_1::text, ', ') as ids,
            string_agg(name2, ', ') as name,
            (array_agg(country))[1] as country,
            --(array_agg(region))[1] as region,
            sum(f2018_pop) as population,
            geom
        FROM
            (SELECT * FROM lion_ranges ORDER BY objectid_1) t
        GROUP BY
            geom;

    -- clean case1 (see email to enrico, 06 May 2019)
    CREATE TEMPORARY TABLE
        lion_ranges_tmp1
    AS
        SELECT
            string_agg(ids, ', ') as ids,
            name,
            country,
            --region, 
            population,
            ST_Union(geom) AS geom
        FROM
            (SELECT * FROM lion_ranges_tmp0 ORDER BY ids) t
        GROUP BY
            name,
            country,
            --region,
            population;

    -- clean case2
    -- 1) create a layer of all clusters (only geom)
    --    and assign a sequential id
    CREATE TEMPORARY TABLE
        lion_ranges_tmp2
    AS
        SELECT
            ST_Buffer(
                ST_UnaryUnion(
                    unnest(
                        ST_ClusterIntersecting(
                            ST_Buffer(
                                geom,
                                ${tolerance}
                            )
                        )
                    )
                ),
                -${tolerance}
            ) AS geom
        FROM
            lion_ranges_tmp1;
    ALTER TABLE
        lion_ranges_tmp2
    ADD COLUMN
        cluster SERIAL PRIMARY KEY;

    -- 2) match geometries to cluster
    ALTER TABLE
        lion_ranges_tmp1
    ADD COLUMN
        cluster INT;
    UPDATE
        lion_ranges_tmp1 l
    SET
        cluster = (
            SELECT
                cluster
            FROM
                lion_ranges_tmp2 c
            WHERE
                ST_Intersects(
                    l.geom,
                    c.geom
                )
        );

    -- 3) group by population count and cluster
    CREATE TEMPORARY TABLE
        lion_ranges_tmp3
    AS
        SELECT
            string_agg(ids, ', ') as ids,
            string_agg(name, ', ') as name,
            string_agg(country, ',') as country,
            --cluster,
            population AS lion_population,
            ST_Area(ST_Union(geom)) / 1000000.0 AS area_km2,
            ST_Union(geom) as geom
        FROM
            (SELECT * FROM lion_ranges_tmp1 ORDER BY ids) t
        GROUP BY
            cluster,
            population;

-- finally: cluster and group adjacent geometries,
-- producing a sum of lion population
DROP TABLE IF EXISTS
    lion_clusters;
CREATE TEMPORARY TABLE
    lion_clusters
AS
    SELECT
        ST_Buffer(
            ST_UnaryUnion(
                unnest(
                    ST_ClusterIntersecting(
                        ST_Buffer(
                            geom,
                            ${tolerance}
                        )
                    )
                )
            ),
            -${tolerance}
        ) AS geom
    FROM
        lion_ranges_tmp3;

ALTER TABLE
    lion_clusters
ADD COLUMN
    cluster SERIAL PRIMARY KEY;

CREATE INDEX ON
    lion_clusters
USING
    GIST(geom);

ALTER TABLE
    lion_ranges_tmp3
ADD COLUMN
    cluster INT;

UPDATE
    lion_ranges_tmp3 l
SET
    cluster = (
        SELECT
            cluster
        FROM
            lion_clusters c
        WHERE
            ST_Intersects(
                l.geom,
                c.geom
            )
    );

DROP TABLE IF EXISTS lion_ranges;
DROP TABLE IF EXISTS "lion ranges";
CREATE TABLE
    "lion ranges"
    AS
        SELECT
            SUM(lion_population)::INT AS lion_population,
            ST_Buffer(
                ST_Union(
                    ST_Buffer(
                        geom,
                        ${tolerance}
                    )
                ),
                -${tolerance}
            ) AS geom
        FROM
            lion_ranges_tmp3
        GROUP BY
            cluster;

ALTER TABLE
    "lion ranges"
ADD COLUMN
    gid SERIAL PRIMARY KEY,
ADD COLUMN
    area_km2 FLOAT,
ADD COLUMN
    centroid geometry('Point', 102022);

UPDATE
    "lion ranges"
SET
    area_km2 = ROUND(
        (
            ST_Area(geom) / 1000000.0
        )::numeric,
        3
    ),
    centroid = ST_Centroid(geom);


CREATE INDEX ON
    "lion ranges"
    USING GIST(geom);           

UPDATE
    "lion ranges"
SET
    geom = ST_Multi(ST_MakeValid(ST_Buffer(geom, 0.0)));
EOF

