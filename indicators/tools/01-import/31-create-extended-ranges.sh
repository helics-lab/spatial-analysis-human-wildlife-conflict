#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

for species in "elephant" "lion"; do

    (
        cat <<EOF | psql conflicts
            -- temporary copy of protected areas
            CREATE TEMPORARY TABLE
                "pa temp"
            AS
                SELECT
                    *
                FROM
                    "protected areas";
            CREATE INDEX ON
                "pa temp"
            USING
                GIST(geom);

            -- group into clusters
            CREATE TEMPORARY TABLE
                "pa + ${species} clusters"
            AS
                SELECT
                    ST_MakeValid(
                        ST_Buffer(
                            ST_UnaryUnion(
                                unnest(
                                   ST_ClusterIntersecting(
                                        ST_Buffer(
                                            geom,
                                            ${tolerance}
                                        )
                                   )
                               )
                           ),
                           -${tolerance}
                        )
                    ) AS geom
                FROM
                    (
                        SELECT
                            geom
                        FROM
                            "protected areas"
                        UNION SELECT
                            geom
                        FROM
                            "${species} ranges"
                    ) t;
            ALTER TABLE
                "pa + ${species} clusters"
            ADD COLUMN
                cluster SERIAL PRIMARY KEY;

            -- assign clusters to input tables
            ALTER TABLE
                "${species} ranges"
            ADD COLUMN
                cluster INT;
            UPDATE
                "${species} ranges" s
            SET
                cluster = (
                    SELECT
                        cluster
                    FROM
                        "pa + ${species} clusters" c
                    WHERE
                        ST_Intersects(
                            s.geom,
                            c.geom
                    )
                );

            -- group (first) species range polygons by
            -- cluster membership (and sum up populations)
            DROP TABLE IF EXISTS
                "${species} extended ranges";
            CREATE TABLE
                "${species} extended ranges"
            AS
                SELECT
                    cluster,
                    SUM("${species}_population") AS "${species}_population",
                    ST_Buffer(  -- maybe here!
                        ST_Union(
                            ST_MakeValid(
                                ST_Buffer(
                                    geom,
                                    ${tolerance}
                                )
                            )
                        ),
                        -${tolerance}
                    ) AS geom
                FROM
                    "${species} ranges"
                GROUP BY
                    cluster;

            -- then add the protected areas around it
            DELETE FROM 
                "${species} extended ranges"
            WHERE
                geom IS NULL
            OR
                "${species}_population" IS NULL
            OR "${species}_population" = 0;

            UPDATE
                "${species} extended ranges"
            SET
                geom = ST_MakeValid(
                    ST_Buffer(
                        geom,
                        0.0
                    )
                );

            WITH
                clustered_pa
            AS (
                SELECT
                    cluster,
                    ST_Union(geom) AS geom
                FROM
                    "pa + ${species} clusters"
                GROUP BY
                    cluster
                )
            UPDATE
                "${species} extended ranges" s
            SET
                geom = 
                    ST_UnaryUnion(
                        ST_Collect(
                            s.geom,
                            p.geom
                        )
                    )
            FROM
                clustered_pa AS p
            WHERE
                p.cluster = s.cluster;
        
            UPDATE
                "${species} extended ranges" s
            SET
                geom = ST_MakeValid(ST_Buffer(geom, 0.0));

            ALTER TABLE 
                "${species} ranges"
            DROP COLUMN
                cluster;
            ALTER TABLE
                "${species} extended ranges"
            ADD COLUMN
                gid SERIAL PRIMARY KEY,
            ADD COLUMN
                area_km2 FLOAT,
            ADD COLUMN
                centroid geometry('Point', 102022);

            UPDATE
                "${species} extended ranges"
            SET
                area_km2 = ROUND(
                    (ST_Area(geom) / 1000000.0)::numeric,
                    3
                ),
                centroid = ST_Centroid(geom),
                geom = ST_Multi(
                    ST_MakeValid(
                        ST_Buffer(
                            geom,
                            0.0
                        )
                    )
                );

EOF

    )&
done

wait
