#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "$dbconnection"

    BEGIN;
    
    DROP SCHEMA IF EXISTS
        speciesrichness
            CASCADE;

    CREATE SCHEMA
        speciesrichness;

    CREATE TABLE
        speciesrichness.herbivores_and_carnivores
        (
            id SERIAL PRIMARY KEY,
            binomial TEXT,
            commonname TEXT,
            category CHAR(2),
            area_km2 FLOAT,
            perimeter_overall_km FLOAT,
            perimeter_with_conflicts_km FLOAT,
            perimeter_without_conflicts_km FLOAT,
            exterior_ring GEOMETRY('LINESTRING', 102022),
            exterior_ring_minus_conflict GEOMETRY('LINESTRING', 102022),
            exterior_ring_X_conflict GEOMETRY('LINESTRING', 102022),
            geom GEOMETRY('MULTIPOLYGON', 102022)
        );

    CREATE INDEX ON
        speciesrichness.herbivores_and_carnivores
    USING
        SPGIST(geom);

    CREATE INDEX ON
        speciesrichness.herbivores_and_carnivores
    USING
        SPGIST(geom);

    INSERT INTO
        speciesrichness.herbivores_and_carnivores
        (
            binomial,
            category,
            geom
        )
        SELECT
            binomial,
            category,
            ST_Multi((ST_Dump(geom)).geom) AS geom
        FROM
            DBLINK(
                'dbname=speciesrichness',
                '
                    SELECT
                        binomial::TEXT,
                        category::CHAR(2),
                        geom
                    FROM
                        data.herbivores_and_carnivores
                '
            )
        AS (
            binomial TEXT,
            category CHAR(2),
            geom GEOMETRY('MULTIPOLYGON', 102022)
        );

    CREATE TEMPORARY TABLE
        clip1
    AS
        WITH
            crops_and_humans AS (
                SELECT
                    geom
                FROM
                    DBLINK(
                        'dbname=speciesrichness',
                        '
                            SELECT
                                geom
                            FROM
                                data.crops
                            UNION SELECT
                                geom
                            FROM
                                data.humpopdens;
                        '
                    ) AS (
                        geom GEOMETRY('POLYGON', 102022)
                    )
            )
        SELECT
           geom::GEOMETRY('POLYGON', 102022) AS geom
        FROM
            crops_and_humans;

    CREATE INDEX ON
        clip1
    USING
        SPGIST(geom);

    CREATE TEMPORARY TABLE
        clip2
    AS
        SELECT
            (ST_Dump(
                ST_Union(geom)
            )).geom AS geom
        FROM
            clip1;

    CREATE INDEX ON
        clip2
    USING
        SPGIST(geom);

   CREATE TEMPORARY TABLE
        clip_union AS (
            SELECT
                h.id,
                ST_Union(c.geom) AS geom
            FROM
                speciesrichness.herbivores_and_carnivores h
            JOIN
                clip2 c
                    ON ST_Intersects(h.geom, c.geom)
            GROUP BY
                h.id
        );

    UPDATE
        clip_union
    SET
        geom = ST_Multi(
            ST_Buffer(
                geom,
                0.0
            )
        );

    UPDATE
        speciesrichness.herbivores_and_carnivores h
    SET
        geom = ST_Multi(
            ST_Difference(
                h.geom,
                c.geom
            )
        )
    FROM
        clip_union c
    WHERE
        h.id = c.id;

    UPDATE
        speciesrichness.herbivores_and_carnivores
    SET
        area_km2 = ST_Area(geom) / 1000000.0,
        perimeter_overall_km = ST_Perimeter(geom) / 1000.0;

    WITH
        all_polygons AS (
            SELECT
                id, 
                (ST_Dump(geom)).geom AS geom
            FROM
                speciesrichness.herbivores_and_carnivores
        ),
        exterior_rings AS (
            SELECT
                id,
                ST_Collect(
                    ST_ExteriorRing(geom)
                ) AS geom
            FROM
                all_polygons
            GROUP BY
                id
        )
    UPDATE
        speciesrichness.herbivores_and_carnivores h
    SET
        exterior_ring = e.geom
    FROM
        exterior_rings e
    WHERE
        h.id = e.id;

    COMMIT;
EOF
