#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

ls -1 ./{01-import,02-analysis}/runall.sh | while read script; do
    
    echo "running ${script}"
    bash "$script"
    exitCode="$?"

    if [[ "${exitCode}" -ne "0" ]];then
        echo "${script} returned ${exitCode}"
        break
    fi
done
