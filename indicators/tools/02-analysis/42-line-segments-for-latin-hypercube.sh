#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"

    DROP SCHEMA IF EXISTS
        sensitivity
            CASCADE;

    CREATE SCHEMA
        sensitivity;

    CREATE TABLE
        sensitivity.segments
        (
            id BIGINT PRIMARY KEY,
            geom GEOMETRY('LINESTRING', 102022),
            length FLOAT,
            country_a3 CHAR(3),
            pa_wdpaid BIGINT
        );
EOF

cat <<EOF | psql "${dbconnection}"
    INSERT INTO
        sensitivity.segments
            (
                id,
                geom,
                length,
                country_a3,
                pa_wdpaid
            )
        SELECT
            id,
            geom,
            length,
            country_a3,
            pa_wdpaid
        FROM
            conflicts.conflicts_number_of_threats;

    CREATE INDEX IF NOT EXISTS
        segments_geom_idx
    ON
        sensitivity.segments
    USING
        SPGIST(geom);

    CREATE INDEX IF NOT EXISTS
        segments_id_idx
    ON
        sensitivity.segments
    USING
        BTREE(id);
EOF

for b in 10 20 30; do
    for q in 90; do
        cat <<EOF | psql "${dbconnection}"
            ALTER TABLE
                sensitivity.segments
            ADD COLUMN
                q${q}_b${b}_priority FENCING_PRIORITY;

            UPDATE
                sensitivity.segments s
            SET
                q${q}_b${b}_priority = f.q${q}_b${b}_priority::FENCING_PRIORITY
            FROM
                conflicts.conflicts_number_of_threats f
            WHERE
                s.id = f.id;
EOF
    done
done
for q in 90; do
    cat <<EOF | psql "${dbconnection}"
        ALTER TABLE
            sensitivity.segments
        ADD COLUMN
            q${q}_min_distance FLOAT,
        ADD COLUMN
            q${q}_max_distance FLOAT,
        ADD COLUMN
            q${q}_mean_distance FLOAT;

        WITH
            ca_distances AS (
                SELECT
                    id,
                    d.distance
                FROM
                    sensitivity.segments s
                    LEFT JOIN
                        distances.cattle_q90 d
                            USING(id)
            ),
            cr_distances AS (
                SELECT
                    id,
                    d.distance
                FROM
                    sensitivity.segments s
                    LEFT JOIN
                        distances.crops_q90 d
                            USING(id)
            ),
            hu_distances AS (
                SELECT
                    id,
                    d.distance
                FROM
                    sensitivity.segments s
                    LEFT JOIN
                        distances.humans_q90 d
                            USING(id)
            ),
            distances AS (
                SELECT
                    id,
                    ca.distance AS ca_distance,
                    cr.distance AS cr_distance,
                    hu.distance AS hu_distance
                FROM
                    ca_distances ca
                    FULL OUTER JOIN
                        cr_distances cr
                            USING(id)
                    FULL OUTER JOIN
                        hu_distances hu
                            USING(id)
            )
        UPDATE
                sensitivity.segments s
        SET
            q${q}_min_distance = LEAST(
                d.ca_distance,
                d.cr_distance,
                d.hu_distance
            ),
            q${q}_max_distance = GREATEST(
                d.ca_distance,
                d.cr_distance,
                d.hu_distance
            ),
            q${q}_mean_distance = (
                (
                    COALESCE(d.ca_distance, 30000)
                    + COALESCE(d.cr_distance, 30000)
                    + COALESCE(d.hu_distance, 30000)
                ) / 3.0
            )
        FROM
            distances d
        WHERE
            s.id = d.id;

EOF
done

cat <<EOF | psql "${dbconnection}"
    DROP TABLE IF EXISTS
        sensitivity.segments_nogeo;

    CREATE TABLE
        sensitivity.segments_nogeo
    AS
        SELECT
            id,
            q90_b10_priority,
            q90_b20_priority,
            q90_b30_priority,
            -- q90_min_distance,
            -- q90_max_distance,
            -- q90_mean_distance,

            -- GREATEST(q90_min_distance - 10000.0, 0.0) AS q90_b10_min_distance,
            -- GREATEST(q90_max_distance - 10000.0, 0.0) AS q90_b10_max_distance,
            -- GREATEST(q90_mean_distance - 10000.0, 0.0) AS q90_b10_mean_distance,
            -- GREATEST(q90_min_distance - 20000.0, 0.0) AS q90_b20_min_distance,
            -- GREATEST(q90_max_distance - 20000.0, 0.0) AS q90_b20_max_distance,
            -- GREATEST(q90_mean_distance - 20000.0, 0.0) AS q90_b20_mean_distance,
            -- GREATEST(q90_min_distance - 30000.0, 0.0) AS q90_b30_min_distance,
            -- GREATEST(q90_max_distance - 30000.0, 0.0) AS q90_b30_max_distance,
            -- GREATEST(q90_mean_distance - 30000.0, 0.0) AS q90_b30_mean_distance

            ROUND(q90_min_distance)::INT AS q90_b10_min_distance,
            ROUND(q90_max_distance)::INT AS q90_b10_max_distance,
            ROUND(q90_mean_distance)::INT AS q90_b10_mean_distance,
            ROUND(q90_min_distance)::INT AS q90_b20_min_distance,
            ROUND(q90_max_distance)::INT AS q90_b20_max_distance,
            ROUND(q90_mean_distance)::INT AS q90_b20_mean_distance,
            ROUND(q90_min_distance)::INT AS q90_b30_min_distance,
            ROUND(q90_max_distance)::INT AS q90_b30_max_distance,
            ROUND(q90_mean_distance)::INT AS q90_b30_mean_distance

        FROM
            sensitivity.segments
        ORDER BY
            id;

        CREATE INDEX
            ON sensitivity.segments_nogeo
            USING btree(q90_b30_min_distance);
        CREATE INDEX
            ON sensitivity.segments_nogeo
            USING btree(q90_b30_max_distance);
        CREATE INDEX
            ON sensitivity.segments_nogeo
            USING btree(q90_b30_mean_distance);
EOF

for m in min max mean; do
    for b in 10 20 30; do
        break
        cat <<EOF | psql "${dbconnection}"
            UPDATE
                sensitivity.segments_nogeo
            SET
                q90_b${b}_${m}_distance = NULL
            WHERE
                q90_b30_${m}_distance > ${b}000.0;
EOF
    done
done

cat <<EOF | psql "${dbconnection}"
    \copy sensitivity.segments_nogeo TO '../../results/sensitivity_segments.csv' CSV HEADER;
EOF
