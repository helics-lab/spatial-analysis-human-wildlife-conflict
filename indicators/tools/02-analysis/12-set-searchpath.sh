#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "$dbconnection"
    CREATE SCHEMA IF NOT EXISTS
        conflicts;

    ALTER DATABASE
        ${dbname}
    SET
        search_path
    TO
        conflicts, data, quantiles, naturalearthdata, public;
EOF
