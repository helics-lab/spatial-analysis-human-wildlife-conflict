#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "$dbconnection"
    ALTER DATABASE
        ${dbname}
    SET
        search_path
    TO
        "\$user", public;
EOF
