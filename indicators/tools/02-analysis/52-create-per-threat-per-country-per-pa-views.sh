#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"
    CREATE SCHEMA IF NOT EXISTS
        report;
EOF

for area in country pa; do
        case "${area}" in
            "country")
                areaTable="naturalearthdata.countries"
                newTable="report.per_country"
                areaId="adm0_a3"
                areaIdType="CHAR(3)"
                joinColumn="country_a3"
                ;;

            "pa")
                areaTable="data.\"protected areas\""
                newTable="report.per_protected_area"
                areaId="wdpaid_int"
                areaIdType="BIGINT"
                joinColumn="pa_wdpaid"
                ;;
        esac

        for q in 70 80 90; do
            for b in 10 20 30; do
                cat <<EOF | psql "${dbconnection}"
                    DROP TABLE IF EXISTS
                        report.conflicts_number_of_threats_q${q}_b${b}_per_${area};
            
                    CREATE TABLE
                        report.conflicts_number_of_threats_q${q}_b${b}_per_${area}
                    AS
                        SELECT
                            ${joinColumn},
                            q${q}_b${b} AS number_of_threats,
                            SUM(length) AS length,
                            q${q}_b${b}_humans AS humans,
                            q${q}_b${b}_cattle AS cattle,
                            q${q}_b${b}_crops AS crops,
                            q${q}_b${b}_priority AS priority
                        FROM
                            conflicts_number_of_threats
                        GROUP BY
                            ${joinColumn},
                            q${q}_b${b},
                            q${q}_b${b}_humans,
                            q${q}_b${b}_cattle,
                            q${q}_b${b}_crops,
                            q${q}_b${b}_priority;
EOF
        done
    done


    for s in elephant lion; do
        (
            cat <<EOF | psql "${dbconnection}"
                DROP TABLE IF EXISTS
                    report.perimeter_of_${s}_ranges_per_${area};

                CREATE TABLE
                    report.perimeter_of_${s}_ranges_per_${area}
                AS
                    SELECT
                        ${joinColumn},
                        SUM(length) AS length
                    FROM
                        report.perimeter_of_${s}_ranges
                    WHERE
                        ${joinColumn} IS NOT NULL
                    GROUP BY
                        ${joinColumn};

            ALTER TABLE
                report.perimeter_of_${s}_ranges_per_${area}
            ADD PRIMARY KEY (${joinColumn});
EOF
        )
    done

    for r in humans cattle crops; do
        for q in 70 80 90; do
            for b in 10 20 30; do
                (
                    cat <<EOF | psql "${dbconnection}"
                        DROP TABLE IF EXISTS
                            report.conflicts_${r}_q${q}_b${b}_per_${area};

                        CREATE TABLE
                            report.conflicts_${r}_q${q}_b${b}_per_${area}
                        AS
                            SELECT
                                ${joinColumn},
                                SUM(length) AS length
                            FROM
                                report.conflicts_${r}_q${q}_b${b}
                            WHERE
                                ${joinColumn} IS NOT NULL
                            GROUP BY
                                ${joinColumn};

                        ALTER TABLE
                            report.conflicts_${r}_q${q}_b${b}_per_${area}
                        ADD PRIMARY KEY
                            (${joinColumn});
EOF
                )
            done
        done
    done

done

wait
