#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

for area in country pa; do
    (
        case "${area}" in
            "country")
                areaTable="naturalearthdata.countries"
                newTable="report.per_country"
                areaId="adm0_a3"
                areaIdType="CHAR(3)"
                joinColumn="country_a3"
                ;;

            "pa")
                areaTable="data.\"protected areas\""
                newTable="report.per_protected_area"
                areaId="wdpaid_int"
                areaIdType="BIGINT"
                joinColumn="pa_wdpaid"
                ;;
        esac

        cat <<EOF | psql "${dbconnection}"
            DROP TABLE IF EXISTS
                ${newTable};

            CREATE TABLE
                ${newTable}
                (
                    ${area}_name TEXT,
                    ${area}_${joinColumn} ${areaIdType} PRIMARY KEY,
                    conflicts_km FLOAT,
                    elephant_population BIGINT,
                    lion_population BIGINT,
                    perimeter_km_elephant_ranges FLOAT,
                    perimeter_km_lion_ranges FLOAT
                );

            INSERT INTO
                ${newTable}
                (
                    ${area}_name,
                    ${area}_${joinColumn}
                )
                SELECT
                    c.name,
                    c.${areaId}
                FROM
                    ${areaTable} c
                ORDER BY
                    c.${areaId} ASC;

            UPDATE
                ${newTable} c
            SET
                conflicts_km = f.length / 1000.0
            FROM
                (
                    SELECT
                        ${joinColumn},
                        SUM(length) AS length
                    FROM
                        conflicts
                    WHERE
                        ${joinColumn} IS NOT NULL
                    GROUP BY
                        ${joinColumn}
                ) f
            WHERE
                c.${area}_${joinColumn} = f.${joinColumn};

            DELETE FROM
                ${newTable}
            WHERE
                conflicts_km IS NULL;

            UPDATE
                ${newTable} c
            SET
                perimeter_km_elephant_ranges = f.length / 1000.0
            FROM
                report.perimeter_of_elephant_ranges_per_${area} f
            WHERE
                c.${area}_${joinColumn} = f.${joinColumn};

            UPDATE
                ${newTable} c
            SET
                perimeter_km_lion_ranges = f.length / 1000.0
            FROM
                report.perimeter_of_lion_ranges_per_${area} f
            WHERE
                c.${area}_${joinColumn} = f.${joinColumn};
EOF
    for species in elephant lion; do
        cat <<EOF | psql "${dbconnection}"
            WITH
                population AS (
                    SELECT
                        c.${areaId} AS ${area}_${joinColumn},
                        (
                            p.${species}_population * (
                                ST_Area(
                                    ST_Intersection(c.geom, p.geom)
                                ) / (
                                    ST_Area(p.geom) + 0.00001
                                )
                            )
                        ) AS population
                    FROM
                        ${areaTable} c,
                        data."${species} ranges" p
                    WHERE
                        c.geom && p.geom
                    AND
                        ST_Intersects(c.geom, p.geom)
                ),
                population_per_country AS (
                    SELECT
                        ${area}_${joinColumn},
                        SUM(population) AS population
                    FROM
                        population
                    GROUP BY
                        ${area}_${joinColumn}
                )
            UPDATE
                ${newTable} c
            SET
                ${species}_population = ROUND(p.population)
            FROM
                population_per_country p
            WHERE
                p.${area}_${joinColumn} = c.${area}_${joinColumn};

            UPDATE
                ${newTable} c
            SET
                ${species}_population = NULL
            WHERE
                ${species}_population = 0;
EOF
    done

    for q in 70 80 90; do
        for b in 10 20 30; do
            for t in 1 2 3; do
                cat <<EOF | psql "${dbconnection}"
                    ALTER TABLE
                        ${newTable}
                    ADD COLUMN
                        km_of_conflict_with_${t}_threats_q${q}_b${b} FLOAT,
                    ADD COLUMN
                        share_of_conflict_with_${t}_threats_q${q}_b${b} FLOAT;

                    UPDATE
                        ${newTable} a
                    SET
                        km_of_conflict_with_${t}_threats_q${q}_b${b} = f.length / 1000.0,
                        share_of_conflict_with_${t}_threats_q${q}_b${b} = (f.length / 1000.0) / a.conflicts_km
                    FROM
                        report.conflicts_number_of_threats_q${q}_b${b}_per_${area} f
                    WHERE
                        f.number_of_threats = ${t}
                    AND
                        a.${area}_${joinColumn} = f.${joinColumn};
EOF
            done
            for t in "low" "moderate" "high" "severe"; do
                cat <<EOF | psql "${dbconnection}"
                    ALTER TABLE
                        ${newTable}
                    ADD COLUMN
                        "km_of_conflict_with_${t}_priority_q${q}_b${b}" FLOAT,
                    ADD COLUMN
                        "share_of_conflict_with_${t}_priority_q${q}_b${b}" FLOAT;

                    UPDATE
                        ${newTable} a
                    SET
                        "km_of_conflict_with_${t}_priority_q${q}_b${b}" = f.length / 1000.0,
                        "share_of_conflict_with_${t}_priority_q${q}_b${b}" = (f.length / 1000.0) / a.conflicts_km
                    FROM
                        report.conflicts_number_of_threats_q${q}_b${b}_per_${area} f
                    WHERE
                        f.priority = '${t}'
                    AND
                        a.${area}_${joinColumn} = f.${joinColumn};
EOF
            done
        done
    done


    echo  "${area} done"

    cat <<EOF | psql "${dbconnection}"
            ALTER TABLE
                ${newTable}
            RENAME COLUMN
                conflicts_km
            TO
                perimeter_of_lion_and_elephant_ranges;
EOF

    )

done
wait
