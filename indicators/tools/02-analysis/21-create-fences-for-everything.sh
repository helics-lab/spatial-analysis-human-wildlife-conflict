#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "$dbconnection"
    BEGIN;

    DROP TABLE IF EXISTS
        conflicts_raw;
    DROP TABLE IF EXISTS
        conflicts;

    CREATE TEMPORARY TABLE
        conflicts_raw
        (
            id BIGSERIAL PRIMARY KEY,
            geom GEOMETRY('LINESTRING', 102022),
            lion BOOLEAN DEFAULT FALSE,
            elephant BOOLEAN DEFAULT FALSE
        );

    INSERT INTO
        conflicts_raw
            (
                geom,
                lion,
                elephant
            )
        (
            SELECT
                (ST_Dump(
                    ST_Segmentize(
                        ST_Boundary(geom),
                        ${tolerance}
                    )
                )).geom,
                (lion_population > 0)::BOOLEAN,
                (elephant_population > 0)::BOOLEAN
            FROM
                data."lions and elephants combined extended ranges"
            WHERE
                (
                    elephant_population > 0
                OR
                    lion_population > 0
                )
        );

    CREATE INDEX
        ON conflicts_raw
        USING GIST(geom);
 
    CREATE TABLE
        conflicts
        (
            id BIGSERIAL PRIMARY KEY,
            country TEXT,
            country_a3 TEXT,
            pa_name TEXT,
            pa_wdpaid BIGINT,
            geom GEOMETRY('LINESTRING', 102022),
            length FLOAT,
            lion BOOLEAN DEFAULT FALSE,
            elephant BOOLEAN DEFAULT FALSE
        );

    CREATE INDEX ON
        conflicts
    USING
        SPGIST(geom);

    CREATE INDEX ON
        conflicts(id)
    WHERE
        lion = TRUE;

    CREATE INDEX ON
        conflicts(id)
    WHERE
        elephant = TRUE;

    INSERT INTO
        conflicts
            (
                geom,
                lion,
                elephant
            )
        (
            WITH
                points
                    AS (
                        SELECT
                            id,
                            (ST_DumpPoints(geom)).geom AS geom
                        FROM
                            conflicts_raw
                    ),
                mpoints
                    AS (
                        SELECT
                            id,
                            ST_Collect(geom) AS geom
                        FROM
                            points
                        GROUP BY
                            id
                    )
            SELECT
                (ST_Dump(
                    ST_Split(
                        l.geom,
                        p.geom
                    )
                )).geom,
                lion,
                elephant
            FROM
                conflicts_raw l
            LEFT JOIN
                mpoints p USING(id)
        );

        UPDATE
            conflicts
        SET
            length = ST_Length(geom);

        UPDATE
            conflicts f
        SET
            country = c.name,
            country_a3 = c.adm0_a3
        FROM
            naturalearthdata.countries c
        WHERE
            f.geom && c.geom
        AND
            ST_Intersects(f.geom, c.geom);

        UPDATE
            conflicts f
        SET
            pa_name = p.name,
            pa_wdpaid = wdpaid_int
        FROM
            data."protected areas" p
        WHERE
            f.geom && p.geom
        AND
            ST_Intersects(f.geom, p.geom);

    WITH
        coastline
            AS (
                SELECT
                    ST_Buffer(
                        ST_Boundary(
                            ST_Union(geom)
                        ),
                        100.0
                    ) AS geom
                FROM
                    naturalearthdata.africa
            )
    DELETE FROM
        conflicts f
    USING
        coastline c
    WHERE
        f.geom && c.geom
    AND
        ST_Intersects(
            f.geom,
            c.geom
        );
    
    COMMIT;
EOF
