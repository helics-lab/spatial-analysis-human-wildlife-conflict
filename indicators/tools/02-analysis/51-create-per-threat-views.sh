#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"
    CREATE SCHEMA IF NOT EXISTS
        report;
EOF

for s in elephant lion; do
    (
        cat <<EOF | psql "${dbconnection}"
            DROP TABLE IF EXISTS
                report.perimeter_of_${s}_ranges
            CASCADE;
            DROP TABLE IF EXISTS
                report.perimeter_of_${s}_ranges;

            CREATE TABLE
                report.perimeter_of_${s}_ranges
            AS
                SELECT
                    *
                FROM
                    conflicts
                WHERE
                    ${s};

            ALTER TABLE
                report.perimeter_of_${s}_ranges
            ADD PRIMARY KEY(id);

            CREATE INDEX ON
                report.perimeter_of_${s}_ranges
                USING GIST(geom);

            CREATE INDEX ON
                report.perimeter_of_${s}_ranges
            USING BTREE(country_a3);

            CREATE INDEX ON
                report.perimeter_of_${s}_ranges
            USING BTREE(pa_wdpaid);
EOF
    )&
done
wait

for r in humans cattle crops; do
    for q in 70 80 90; do
        for b in 10 20 30; do
            (
                cat <<EOF | psql "${dbconnection}"
                    DROP TABLE IF EXISTS
                        report.conflicts_${r}_q${q}_b${b} 
                    CASCADE;
                    DROP TABLE IF EXISTS
                        report.conflicts_${r}_q${q}_b${b};

                    CREATE TABLE
                        report.conflicts_${r}_q${q}_b${b}
                    AS
                        SELECT
                            *
                        FROM
                            conflicts.conflicts_number_of_threats
                        WHERE
                            q${q}_b${b}_${r};

                    ALTER TABLE
                        report.conflicts_${r}_q${q}_b${b}
                    ADD PRIMARY KEY (id);
            
                    CREATE INDEX ON
                        report.conflicts_${r}_q${q}_b${b}
                        USING GIST(geom);

                    CREATE INDEX ON
                        report.conflicts_${r}_q${q}_b${b}
                    USING BTREE(country_a3);
        
                    CREATE INDEX ON
                        report.conflicts_${r}_q${q}_b${b}
                    USING BTREE(pa_wdpaid);
EOF
            )&
        done
    done
done

wait
