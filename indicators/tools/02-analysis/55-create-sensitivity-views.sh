#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"
    DROP TABLE IF EXISTS
        report.conflict_line_length_per_country;

    CREATE TABLE
        report.conflict_line_length_per_country
    AS
        SELECT
            country_name,
            km_of_conflict_with_severe_priority_q70_b10,
            km_of_conflict_with_severe_priority_q70_b20,
            km_of_conflict_with_severe_priority_q70_b30,
            km_of_conflict_with_severe_priority_q80_b10,
            km_of_conflict_with_severe_priority_q80_b20,
            km_of_conflict_with_severe_priority_q80_b30,
            km_of_conflict_with_severe_priority_q90_b10,
            km_of_conflict_with_severe_priority_q90_b20,
            km_of_conflict_with_severe_priority_q90_b30
        FROM
            report.per_country;
EOF
