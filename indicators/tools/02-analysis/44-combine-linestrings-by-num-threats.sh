#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh



for q in 70 80 90; do
    for b in 10 20 30; do
        (
            cat <<EOF | psql "${dbconnection}"
                DROP TABLE IF EXISTS
                    conflicts_q${q}_b${b};

                CREATE TABLE
                    conflicts_q${q}_b${b}
                    (
                        id BIGSERIAL PRIMARY KEY,
                        geom GEOMETRY('LINESTRING'),
                        simplified_geom GEOMETRY('LINESTRING'),
                        humans BOOLEAN DEFAULT FALSE,
                        cattle BOOLEAN DEFAULT FALSE,
                        crops BOOLEAN DEFAULT FALSE,
                        number_of_threats INT,
                        priority FENCING_PRIORITY
                    );

                INSERT INTO
                    conflicts_q${q}_b${b}
                    (
                        geom,
                        humans,
                        cattle,
                        crops,
                        number_of_threats,
                        priority
                    )
                SELECT
                    (ST_Dump(
                        ST_LineMerge(
                            ST_Multi(
                                ST_UnaryUnion(
                                    unnest(
                                        ST_ClusterIntersecting(geom)
                                    )
                                )
                            )
                        )
                    )).geom,
                    q${q}_b${b}_humans,
                    q${q}_b${b}_cattle,
                    q${q}_b${b}_crops,
                    q${q}_b${b},
                    q${q}_b${b}_priority
                FROM
                    conflicts_number_of_threats
                GROUP BY
                    q${q}_b${b},
                    q${q}_b${b}_humans,
                    q${q}_b${b}_cattle,
                    q${q}_b${b}_crops,
                    q${q}_b${b}_priority;


                CREATE INDEX ON
                    conflicts_q${q}_b${b}
                USING
                    SPGIST(geom);

                UPDATE
                    conflicts_q${q}_b${b}
                SET simplified_geom = ST_SimplifyPreserveTopology(
                    geom,
                    ${tolerance} * 100.0
                );

                CREATE INDEX ON
                    conflicts_q${q}_b${b}
                USING
                    SPGIST(simplified_geom);

EOF
        ) #&
    done
done

wait
