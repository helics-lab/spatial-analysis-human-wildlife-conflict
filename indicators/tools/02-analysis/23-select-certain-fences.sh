#!/bin/bash

set +m

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

for threat in humans cattle crops; do
    for q in 70 80 90; do
        for b in 10 20 30; do
            cat <<EOF | psql "${dbconnection}"
                ALTER TABLE
                    conflicts
                DROP COLUMN
                    ${threat}_q${q}_b${b};

                ALTER TABLE
                    conflicts
                ADD COLUMN
                    ${threat}_q${q}_b${b} BOOLEAN DEFAULT FALSE;
EOF
        done
    done
done

for threat in humans cattle crops; do
    for q in 70 80 90; do
        for b in 10 20 30; do
            (
                cat <<EOF | psql "${dbconnection}"
                    CREATE INDEX IF NOT EXISTS
                        conflicts_id_${threat}_q${q}_b${b}_idx
                    ON
                        conflicts(id)
                    WHERE
                        ${threat}_q${q}_b${b} = TRUE;

                    BEGIN;

                        EXPLAIN ANALYSE UPDATE
                            conflicts f
                        SET
                            ${threat}_q${q}_b${b} = TRUE
                        FROM
                            distances.${threat}_q${q} d
                        WHERE
                            f.id = d.id
                        AND
                            d.distance <= ${b}000;

                    COMMIT;
EOF
            ) #&
        done
    done
done
wait
