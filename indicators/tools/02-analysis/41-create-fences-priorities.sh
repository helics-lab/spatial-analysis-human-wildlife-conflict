#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"


    DROP TABLE IF EXISTS
        conflicts_number_of_threats
            CASCADE;

    CREATE TABLE
        conflicts_number_of_threats
        (
            id BIGINT PRIMARY KEY,
            geom GEOMETRY('LINESTRING', 102022),
            length FLOAT,
            country_a3 CHAR(3),
            pa_wdpaid BIGINT
        );

    INSERT INTO
        conflicts_number_of_threats
        (
            id,
            geom,
            length,
            country_a3,
            pa_wdpaid
        )
        SELECT
            id,
            geom,
            length,
            country_a3,
            pa_wdpaid
        FROM
            conflicts;
EOF

for q in 70 80 90; do
    for b in 10 20 30; do
        cat <<EOF | psql "${dbconnection}"
            ALTER TABLE
                conflicts_number_of_threats
            ADD COLUMN
                q${q}_b${b} INT,
            ADD COLUMN
                q${q}_b${b}_humans BOOLEAN DEFAULT FALSE,
            ADD COLUMN
                q${q}_b${b}_cattle BOOLEAN DEFAULT FALSE,
            ADD COLUMN
                q${q}_b${b}_crops BOOLEAN DEFAULT FALSE,
            ADD COLUMN
                q${q}_b${b}_priority FENCING_PRIORITY DEFAULT NULL;

            UPDATE
                conflicts_number_of_threats fnt
            SET
                q${q}_b${b} =
                    (f.humans_q${q}_b${b} AND (f.lion OR f.elephant))::INT
                    + (f.cattle_q${q}_b${b} AND f.lion)::INT
                    + (f.crops_q${q}_b${b} AND f.elephant)::INT,
                q${q}_b${b}_humans = f.humans_q${q}_b${b} AND (f.lion OR f.elephant),
                q${q}_b${b}_cattle = f.cattle_q${q}_b${b} AND f.lion,
                q${q}_b${b}_crops = f.crops_q${q}_b${b} AND f.elephant
            FROM
                conflicts f
            WHERE
                f.id = fnt.id;

                UPDATE
                    conflicts_number_of_threats
                SET
                    q${q}_b${b}_priority = 'low'
                WHERE
                    q${q}_b${b} = 1;

                UPDATE
                    conflicts_number_of_threats
                SET
                    q${q}_b${b}_priority = 'moderate'
                WHERE
                    q${q}_b${b} = 2
                AND
                    NOT q${q}_b${b}_humans;

                UPDATE
                    conflicts_number_of_threats
                SET
                    q${q}_b${b}_priority = 'high'
                WHERE
                    q${q}_b${b} = 2
                AND
                    q${q}_b${b}_humans;

                UPDATE
                    conflicts_number_of_threats
                SET
                    q${q}_b${b}_priority = 'severe'
                WHERE
                    q${q}_b${b} = 3;

            CREATE INDEX ON
                conflicts_number_of_threats (q${q}_b${b});
                
EOF
    done
done
