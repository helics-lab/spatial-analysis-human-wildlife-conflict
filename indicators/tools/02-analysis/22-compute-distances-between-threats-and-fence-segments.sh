#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"
    DROP SCHEMA IF EXISTS
        distances
            CASCADE;
    CREATE SCHEMA
        distances;

EOF

for threat in humans cattle crops; do
    for q in 70 80 90; do
        for b in 10 20 30; do
            cat <<EOF | psql "${dbconnection}"
                ALTER TABLE
                    conflicts
                ADD COLUMN
                    ${threat}_q${q}_b${b} BOOLEAN DEFAULT FALSE;

                CREATE INDEX ON
                    conflicts(id)
                WHERE
                    ${threat}_q${q}_b${b} = TRUE;
EOF
        done
    done
done

for threat in humans cattle crops; do
    for q in 70 80 90; do
       (
           cat <<EOF | psql "${dbconnection}"
                ALTER FUNCTION ST_Distance(geometry, geometry) COST 100000;

                DROP TABLE IF EXISTS
                    distances.${threat}_q${q};

                CREATE TABLE
                    distances.${threat}_q${q}
                AS
                    WITH
                        min_distances AS (
                            SELECT
                                f.id,
                                MIN(
                                    ST_Distance(
                                        f.geom,
                                        q.geom
                                    )
                                ) AS distance
                            FROM
                                conflicts f,
                                quantiles.${threat}_q${q} q
                            WHERE
                                f.geom && ST_Expand(q.geom, 30000.0)  -- 30000.0 == max buffer width
                            GROUP BY
                                f.id
                    )
                    SELECT
                        id,
                        distance
                    FROM
                        min_distances
                    WHERE
                        distance <= 30000.0;

                CREATE INDEX ON
                    distances.${threat}_q${q}
                USING BTREE(id);

                CREATE INDEX ON
                    distances.${threat}_q${q} (id)
                    WHERE (
                        distance <= 10000.0
                    );

                CREATE INDEX ON
                    distances.${threat}_q${q} (id)
                    WHERE (
                        distance <= 20000.0
                    );

                CREATE INDEX ON
                    distances.${threat}_q${q} (id)
                    WHERE (
                        distance <= 30000.0
                    );
EOF
        )&
    done
done
wait
