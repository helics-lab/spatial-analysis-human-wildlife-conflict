#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

mkdir -p "../../results/datarepository"

cat <<EOF | psql "${dbconnection}"
    DROP SCHEMA IF EXISTS
        datarepository
            CASCADE;

    CREATE SCHEMA
        datarepository;

    ALTER DATABASE
        ${dbname}
    SET
        search_path
    TO
        datarepository, conflicts, public;
EOF

for b in \
    10 \
    20 \
    30
do
    (
        cat <<EOF | psql "${dbconnection}"
            CREATE TABLE
                "Risk_of_conflict_at_${b}_km_buffer_size"
                (
                    fid SERIAL PRIMARY KEY,
                    geom GEOMETRY('LINESTRING', 4326),
                    "Risk of conflict" TEXT
                );
            CREATE INDEX ON
                "Risk_of_conflict_at_${b}_km_buffer_size"
            USING
                SPGIST(geom);

            INSERT INTO
                "Risk_of_conflict_at_${b}_km_buffer_size"
                (
                    geom,
                    "Risk of conflict"
                )
                (
                    SELECT
                        ST_Transform(
                            geom,
                            4326
                        ),
                        q90_b${b}_priority::TEXT
                    FROM
                        conflicts_number_of_threats
                    WHERE
                        q90_b${b}_priority IS NOT NULL
                );
EOF

        ogr2ogr \
            -progress \
            -f GPKG \
            -overwrite \
            "../../results/datarepository/Risk of conflict (at ${b} km buffer size).gpkg" \
            -a_srs "EPSG:4326" \
            'PG:dbname=conflicts' \
            -oo ACTIVE_SCHEMA=datarepository \
            -lco IDENTIFIER="Risk of conflict (at ${b} km buffer size)" \
            -lco DESCRIPTION="Supplementary data for Di Minin et al. 2021" \
            -nln "Risk of conflict (at ${b} km buffer size)" \
        	"Risk_of_conflict_at_${b}_km_buffer_size"

#        zip \
#            -9 \
#            "../../results/datarepository/Risk of conflict (at ${b} km buffer size).gpkg.zip" \
#            "../../results/datarepository/Risk of conflict (at ${b} km buffer size).gpkg"

        ida \
            upload \
            -v \
            "/spatial_analysis_human_wildlife_conflict/Risk of conflict (at ${b} km buffer size).gpkg" \
            "../../results/datarepository/Risk of conflict (at ${b} km buffer size).gpkg"
    )&
done
wait
