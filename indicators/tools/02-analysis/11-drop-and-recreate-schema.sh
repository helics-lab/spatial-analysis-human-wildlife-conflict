#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "$dbconnection"
    DROP SCHEMA IF EXISTS
        conflicts
    CASCADE;

    CREATE SCHEMA
        conflicts;
    
    CREATE TYPE
        FENCING_PRIORITY AS ENUM(
            'low',
            'moderate',
            'high',
            'severe'
        );
EOF
