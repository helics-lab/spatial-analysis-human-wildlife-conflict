#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

mkdir -p log

ls -1 [1-8][0-9]*.sh | while read script; do
   
    started=$(date +'%s')
    
    date +"running ${script} %Y-%m-%d %H:%M:%S"
    date +"%n%nrunning ${script} %Y-%m-%d %H:%M:%S" >> "log/${script}.log"
    bash "$script" 2>&1 >> "log/${script}.log"
    exitCode="$?"

    duration=$(expr $(date +'%s') - ${started})
    echo "${script} took ${duration} seconds"
    date +"finished ${script} %Y-%m-%d %H:%M:%S (took ${duration} seconds)" >> "log/${script}.log"

    if [[ "${exitCode}" -ne "0" ]];then
        echo "${script} returned ${exitCode}"
        break
    fi
done
