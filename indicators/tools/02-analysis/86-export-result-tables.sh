#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

mkdir -p "../../results"

cat <<EOF | psql "${dbconnection}"
    \copy report.per_country TO '../../results/per_country.csv' CSV HEADER;
    \copy report.per_protected_area TO '../../results/per_protected_area.csv' CSV HEADER;
    \copy report.conflict_line_length_per_country TO '../../results/conflict_line_length_per_country.csv' CSV HEADER;
EOF

ogr2ogr \
    -progress \
    -f GPKG \
    -overwrite \
    ../../results/conflicts.gpkg \
    -a_srs "ESRI:102022" \
    'PG:dbname=conflicts' \
	conflicts_q70_b10 \
	conflicts_q70_b20 \
	conflicts_q70_b30 \
	conflicts_q80_b10 \
	conflicts_q80_b20 \
	conflicts_q80_b30 \
	conflicts_q90_b10 \
	conflicts_q90_b20 \
	conflicts_q90_b30

ogr2ogr \
    -progress \
    -f GPKG \
    -overwrite \
    ../../results/conflicts_all_layers.gpkg \
    -a_srs "ESRI:102022" \
    'PG:dbname=conflicts'
