#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"
    DROP TABLE IF EXISTS
        sensitivity.segments_nogeo_capped;

    CREATE TABLE
        sensitivity.segments_nogeo_capped
    AS
        SELECT
            id,
            q90_b10_priority,
            q90_b20_priority,
            q90_b30_priority,
            -- q90_min_distance,
            -- q90_max_distance,
            -- q90_mean_distance,

            -- GREATEST(q90_min_distance - 10000.0, 0.0) AS q90_b10_min_distance,
            -- GREATEST(q90_max_distance - 10000.0, 0.0) AS q90_b10_max_distance,
            -- GREATEST(q90_mean_distance - 10000.0, 0.0) AS q90_b10_mean_distance,
            -- GREATEST(q90_min_distance - 20000.0, 0.0) AS q90_b20_min_distance,
            -- GREATEST(q90_max_distance - 20000.0, 0.0) AS q90_b20_max_distance,
            -- GREATEST(q90_mean_distance - 20000.0, 0.0) AS q90_b20_mean_distance,
            -- GREATEST(q90_min_distance - 30000.0, 0.0) AS q90_b30_min_distance,
            -- GREATEST(q90_max_distance - 30000.0, 0.0) AS q90_b30_max_distance,
            -- GREATEST(q90_mean_distance - 30000.0, 0.0) AS q90_b30_mean_distance

            ROUND(q90_min_distance)::INT AS q90_b10_min_distance,
            ROUND(q90_max_distance)::INT AS q90_b10_max_distance,
            ROUND(q90_mean_distance)::INT AS q90_b10_mean_distance,
            ROUND(q90_min_distance)::INT AS q90_b20_min_distance,
            ROUND(q90_max_distance)::INT AS q90_b20_max_distance,
            ROUND(q90_mean_distance)::INT AS q90_b20_mean_distance,
            ROUND(q90_min_distance)::INT AS q90_b30_min_distance,
            ROUND(q90_max_distance)::INT AS q90_b30_max_distance,
            ROUND(q90_mean_distance)::INT AS q90_b30_mean_distance

        FROM
            sensitivity.segments
        ORDER BY
            id ASC;

        CREATE INDEX
            ON sensitivity.segments_nogeo_capped
            USING btree(q90_b10_min_distance);
        CREATE INDEX
            ON sensitivity.segments_nogeo_capped
            USING btree(q90_b10_max_distance);
        CREATE INDEX
            ON sensitivity.segments_nogeo_capped
            USING btree(q90_b10_mean_distance);
EOF

for m in min max mean; do
    for b in 30 20 10; do
        cat <<EOF | psql "${dbconnection}"
            UPDATE
                sensitivity.segments_nogeo_capped
            SET
                q90_b${b}_${m}_distance = ${b}000.0
            WHERE
                q90_b10_${m}_distance IS NULL
            OR
                q90_b10_${m}_distance > ${b}000.0;
EOF
    done
done

cat <<EOF | psql "${dbconnection}"
    \copy sensitivity.segments_nogeo_capped TO '../../results/sensitivity_segments_capped.csv' CSV HEADER;
EOF
