#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh


mkdir -p "../../data/psql-dump/"
pg_dump -cCOx "$dbname" | pbzip2 -c -m1000 > "$(date +"../../data/psql-dump/${dbname}_%Y%m%d_%H%M.sql.bz2")"
