#!/bin/bash

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"
    DROP SCHEMA IF EXISTS
        costs
            CASCADE;
    CREATE SCHEMA costs;

    CREATE INDEX IF NOT EXISTS
        conflicts_number_of_threats_country_a3_idx
    ON
        conflicts.conflicts_number_of_threats
    USING BTREE(country_a3);

EOF

for t in "high" "severe"; do
    for b in 10 20 30; do
        (
            cat <<EOF | psql "${dbconnection}"
                CREATE INDEX IF NOT EXISTS
                    conflicts_number_of_threats_q90_b${b}_${t}_idx
                ON
                    conflicts.conflicts_number_of_threats (id)
                WHERE
                    q90_b${b}_priority = '${t}';

                CREATE TABLE
                    costs."${t}_${b}"
                    (
                        id BIGSERIAL PRIMARY KEY,
                        geom GEOMETRY('MULTILINESTRING'),
                        buffer GEOMETRY('MULTIPOLYGON'),
                        country_a3 CHAR(3),
                        humans_no FLOAT,
                        cattle_no FLOAT,
                        crops_km2 FLOAT
                    );

                INSERT INTO
                    costs."${t}_${b}"
                    (
                        geom,
                        buffer,
                        country_a3
                    )
                    (
                        SELECT
                            ST_Multi(
                                ST_Collect(geom)
                            ),
                            ST_Multi(
                                ST_Union(
                                    ST_Buffer(
                                        geom,
                                        ${b}000.0
                                    )
                                )
                            ),
                            country_a3
                        FROM
                            conflicts.conflicts_number_of_threats
                        WHERE
                            q90_b10_priority = '${t}'
                        GROUP BY
                            country_a3
                    );

                CREATE INDEX
                    "${t}_${b}_buffer_idx"
                ON
                    costs."${t}_${b}"
                USING
                    SPGIST(buffer);

                WITH
                    overlapping_protected_areas AS (
                        SELECT
                            c.id,
                            ST_Buffer(
                                ST_Union(
                                    p.geom
                                ),
                                0.0
                            ) AS geom
                        FROM
                            costs."${t}_${b}" c,
                            data."protected areas" p
                        WHERE
                            c.buffer && p.geom
                        GROUP BY
                            c.id
                    )
                UPDATE
                    costs."${t}_${b}" c
                SET
                    buffer = ST_Multi(ST_Difference(c.buffer, o.geom))
                FROM
                    overlapping_protected_areas o
                WHERE
                    c.id = o.id;

                WITH
                    overlapping_range_in_sds_or_caf AS (
                        SELECT
                            c.id,
                            ST_Buffer(
                                ST_Union(
                                    r.geom
                                ),
                                0.0
                            ) AS geom
                        FROM
                            costs."${t}_${b}" c,
                            data."lions and elephants combined ranges" r
                        WHERE
                            (
                                c.country_a3 = 'CAF'
                                OR c.country_a3 = 'SDS'
                            )
                        AND
                            c.buffer && r.geom
                        GROUP BY
                            c.id
                    )
                UPDATE
                    costs."${t}_${b}" c
                SET
                    buffer = ST_Multi(ST_Difference(c.buffer, o.geom))
                FROM
                    overlapping_range_in_sds_or_caf o
                WHERE
                    c.id = o.id;

                UPDATE
                    costs."${t}_${b}"
                SET
                    buffer = ST_Multi(ST_Buffer(ST_Transform(buffer, 4326), 0.0));

                WITH
                    humans_ AS (
                        SELECT 
                            v.id,
                            (
                                ST_SummaryStats(
                                    ST_Clip(
                                        ST_SetSRID(
                                            r.rast,
                                            4326
                                        ),
                                        v.buffer
                                    )
                                )
                            ).SUM AS humans_no
                        FROM
                            data.humans r,
                            costs."${t}_${b}" v
                        WHERE
                            v.buffer && r.rast
                    ),
                    humans AS (
                        SELECT
                            id,
                            SUM(humans_no) AS humans_no
                        FROM
                            humans_
                        GROUP BY
                            id
                    )
                UPDATE
                    costs."${t}_${b}" co
                SET
                    humans_no = hu.humans_no
                FROM
                    humans hu
                WHERE
                    co.id = hu.id;

                WITH
                    cattle_ AS (
                        SELECT 
                            v.id,
                            (
                                ST_SummaryStats(
                                    ST_Clip(
                                        ST_SetSRID(
                                            r.rast,
                                            4326
                                        ),
                                        v.buffer
                                    )
                                )
                            ).SUM AS cattle_no
                        FROM
                            data.cattle r,
                            costs."${t}_${b}" v
                        WHERE
                            v.buffer && r.rast
                    ),
                    cattle AS (
                        SELECT
                            id,
                            SUM(cattle_no) AS cattle_no
                        FROM
                            cattle_
                        GROUP BY
                            id
                    )
                UPDATE
                    costs."${t}_${b}" co
                SET
                    cattle_no = ca.cattle_no
                FROM
                    cattle ca
                WHERE
                    co.id = ca.id;

                WITH
                    crops_ AS (
                        SELECT
                            v.id,
                            (
                                ST_SummaryStats(
                                    ST_Clip(
                                        ST_SetSRID(
                                            r.rast,
                                            4326
                                        ),
                                        v.buffer
                                    )
                                )
                            ).MEAN / 100.0 AS crops_ratio
                        FROM
                            data.crops r,
                            costs."${t}_${b}" v
                        WHERE
                            v.buffer && r.rast
                    ),
                    crops__ AS (
                        SELECT
                            id,
                            AVG(crops_ratio) AS crops_ratio
                        FROM
                            crops_
                        GROUP BY
                            id
                    ),
                    crops AS (
                        SELECT
                            co.id,
                            (
                                (
                                    ST_Area(
                                        ST_Transform(
                                            co.buffer,
                                            102022
                                        )
                                    ) / 1000000
                                ) * cr.crops_ratio
                            ) AS crops_km2
                        FROM
                            costs."${t}_${b}" co
                            JOIN
                                crops__ cr
                                    USING(id)
                    )
                UPDATE
                    costs."${t}_${b}" co
                SET
                    crops_km2 = cr.crops_km2
                FROM
                    crops cr
                WHERE
                    co.id = cr.id
EOF
        )&
    done
done

wait

cat <<EOF | psql "${dbconnection}"
    CREATE TABLE
        costs.per_country
        (
            country_a3 CHAR(3)
        );

    INSERT INTO
        costs.per_country
        (
            country_a3
        )
        (
            SELECT
                DISTINCT country_a3
            FROM
                conflicts.conflicts_number_of_threats
            WHERE
                country_a3 IS NOT NULL
            ORDER BY
                country_a3
        );
EOF

for t in "high" "severe"; do
    for b in 10 20 30; do
        cat <<EOF | psql "${dbconnection}"
            ALTER TABLE
                costs.per_country
            ADD COLUMN
                "humans_no_${t}_b${b}" FLOAT,
            ADD COLUMN
                "cattle_no_${t}_b${b}" FLOAT,
            ADD COLUMN
                "crops_km2_${t}_b${b}" FLOAT;

            WITH
                sums AS (
                    SELECT
                        country_a3,
                        SUM(humans_no) AS humans_no,
                        SUM(cattle_no) AS cattle_no,
                        SUM(crops_km2) AS crops_km2
                    FROM
                        costs."${t}_${b}"
                    GROUP BY
                        country_a3
                )
            UPDATE
                costs.per_country c
            SET
                "humans_no_${t}_b${b}" = s.humans_no,
                "cattle_no_${t}_b${b}" = s.cattle_no,
                "crops_km2_${t}_b${b}" = s.crops_km2
            FROM
                sums s
            WHERE
                c.country_a3 = s.country_a3;
EOF
    done
done

cat <<EOF | psql "${dbconnection}"
    \COPY costs.per_country TO '../../results/costs_per_country.csv' CSV HEADER;
EOF
