#!/bin/bash

# tolerance in meters for combining geometries
export tolerance=100.0

# database connection string
export dbname="conflicts"
export dbconnection="dbname=${dbname}"

export availableRam="$(awk '/MemAvailable/ { printf "%d \n", $2/1024 }' /proc/meminfo)"
