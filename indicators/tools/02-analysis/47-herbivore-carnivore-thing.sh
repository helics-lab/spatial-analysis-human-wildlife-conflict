#!/bin/bash

set +m

origWD="$(pwd)"
cd "$(dirname "${0}")"

source 00-configuration.sh

cat <<EOF | psql "${dbconnection}"
    WITH
        conflicts_buffered AS (
            SELECT
                ST_Buffer(
                    geom,
                    20000,
                    'endcap=flat'
                ) AS geom
            FROM
                conflicts_q90_b10
            WHERE
                priority = 'severe'
        ),
        overlapping_conflicts AS (
            SELECT
                h.id,
                ST_Union(
                    f.geom
                ) AS geom
            FROM
                speciesrichness.herbivores_and_carnivores h,
                conflicts_buffered f
            WHERE
                h.geom && f.geom
            AND
                ST_Intersects(
                    h.exterior_ring,
                    f.geom
                )
            GROUP BY
                h.id,
                h.geom,
                h.exterior_ring
        )
    UPDATE
        speciesrichness.herbivores_and_carnivores h
    SET
        exterior_ring_minus_conflict = ST_Multi(
            ST_Difference(
                exterior_ring,
                COALESCE(f.geom, ST_GeomFromText('LINESTRING EMPTY'))
            )
        ),
        exterior_ring_X_conflict = ST_Multi(
            ST_Intersection(
                exterior_ring,
               COALESCE(f.geom, ST_GeomFromText('LINESTRING EMPTY'))
            )
        )
    FROM
        overlapping_conflicts f
    WHERE
        h.id = f.id;

    UPDATE
        speciesrichness.herbivores_and_carnivores h
    SET
        perimeter_with_conflicts_km = ST_Length(exterior_ring_X_conflict) / 1000.0,
        perimeter_without_conflicts_km = ST_Length(exterior_ring_minus_conflict) / 1000.0;

    CREATE TEMPORARY TABLE
        report
        AS
            SELECT
                commonname || ' (' || binomial || ')' AS "Species",
                category AS "IUCN Category",
                ROUND(SUM(area_km2) * 10.0) / 10.0 AS "Area (km²)",
                ROUND(SUM(perimeter_overall_km) * 10.0) / 10.0 AS "Without conflicts\nPerimeter (km)",
                ROUND(SUM(perimeter_overall_km) / SUM(area_km2) * 1000.0) / 1000.0 AS "Ratio",
                ROUND(SUM(perimeter_without_conflicts_km) * 10.0) / 10.0 AS "With conflicts\nPerimeter (km)",
                ROUND(SUM(perimeter_without_conflicts_km) / SUM(area_km2) * 1000.0) / 1000.0 AS "Ratio_"
            FROM
                speciesrichness.herbivores_and_carnivores
            GROUP BY
                commonname,
                binomial,
                category
            ORDER BY
                "Ratio";

    \COPY report TO '../../results/herbivores_carnivores.csv' CSV HEADER; 
EOF
wait
